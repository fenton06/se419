1.
	
	//Use combiner - will gather all the key data for each value and send once instead of multiple times
	job.setCombinerClass(Reduce1.class)
	
	Map1 (){
		Load data
		Emit key as symbol, value as (date dividends)
	}
	
	Reduce1() {
		echo Map1 // Write
	}
	
	Map2() {
		Compare dates to date1 and date 2
		Emit key as symbol and value as (date1 divs_date1 date2 divs_date2)
	}
	
	Reduce2(){
		if(divs_date1 < divs_date2) {
			write key as symbol
		}
	}

2.

	a.
	
	Skewed join - Computes a histogram of key space and uses this data to allocate reducers for a given key
	
	If there is a single key value that has most of the joins occurring within it (such as 90% of a service provider's 
	business is a single service) and the providers table has millions of tuples (customers, containing a service id.)
	Then there is a customer table(which are companies) with a couple hundred tuples of the services they use 
	(also contains a service id).
	
	The skewed join will help allocate the key containing a majority of the joins to increase parallelism and not bog down the process.
	
	b.
	
	Replicated Join - Works well if a relation is small enough to fit into main memory, because all hadoop work is done in Map phase.
	
	Building off the example above, if the data provider all serve customers with an even distribution, we don't need
	to use a skewed join.  The customer table being small will fit into main memory, and all the joins can happen very quickly.

3.
	a.
	
	SELECT S.sname
	FROM Sailors S, Reservations R
	WHERE S.sid = R.sid AND R.day = 3/15/2017
	
	b.
	
	SELECT R.bid, COUNT(*)
	FROM Reservations R
	GROUPBY R.bid

4.

	HBase - Facebook:
	
		a: Data are chats, among other messaging services
		
		b: Used to store messages between users
		
		c:
			Advantages:
				High write throughput
				Good random read performance
				Automatic failover
				Benefits of HDFS (such as fault tolerant, checksums, MapReduce)
				
			Disadvantages:
				No join operations
				Tough to query
				Only one indexing table
				Expensive hardware requirements and memory block allocations
				
	Hive - Also Facebook!
	
		a: click counts, user engagement, microstrategy reports
		
		b: Reporting data
		
		c:
		
			Advantages:
				Less time to write Hive queries than SQL queries
				Very easy to write join queries
				
			Disadvantages:
				Only good for structured data
				Debugging code is difficult
