import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class Exp2 extends Configured implements Tool {

    private static String input;
    private static String output;

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Requires 2 arguments arg0=inputPath and arg1=outputPath.  Only " + args.length + " were specified.");
            System.exit(-1);
        }

        input = args[0];
        output = args[1];

        int res = ToolRunner.run(new Configuration(), new Exp2(), args);

        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {

        // Create conf
        Configuration conf = new Configuration();

        // Delete previous output
        FileSystem.get(conf).delete(new Path(output), true);

        // Job 1
        Job job_one = Job.getInstance(conf, "Exp2");
        job_one.setJarByClass(Exp2.class);

        job_one.setMapperClass(Map_One.class);

        job_one.setReducerClass(Reduce_One.class);
        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(IntWritable.class);

        job_one.setInputFormatClass(JSONInputFormat.class);
        job_one.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_one, new Path(input));
        FileOutputFormat.setOutputPath(job_one, new Path(output));

        job_one.waitForCompletion(true);

        return 0;
    }

    public static class Map_One extends Mapper<LongWritable, Text, Text, IntWritable> {

        private JSONParser parser = new JSONParser();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            try {
                // Create JSONObject
                Object obj = parser.parse(value.toString());
                JSONObject jsonObj = (JSONObject) obj;

                // Get screen_name and followers_count from user object
                JSONObject user = (JSONObject) jsonObj.get("user");

                String screen_name = user.get("screen_name").toString();
                int followers_count = Integer.parseInt(user.get("followers_count").toString());

                // Write to context
                context.write(new Text(screen_name), new IntWritable(followers_count));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public static class Reduce_One extends Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

            // Find max follower count
            int maxFollowers = 0;

            // Add the 1's to get number of times used
            for (IntWritable valWriteable : values) {

                int val = valWriteable.get();

                if(val > maxFollowers) {
                    maxFollowers = val;
                }
            }

            context.write(key, new IntWritable(maxFollowers));
        }
    }

}
