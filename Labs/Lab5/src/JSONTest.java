import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

public class JSONTest extends Configured implements Tool {

    private static String input;
    private static String output;

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Requires 2 arguments arg0=inputPath and arg1=outputPath.  Only " + args.length + " were specified.");
            System.exit(-1);
        }

        input = args[0];
        output = args[1];

        int res = ToolRunner.run(new Configuration(), new JSONTest(), args);

        System.exit(res);

    }

    @Override
    public int run(String[] args) throws Exception {

        // Create conf
        Configuration conf = new Configuration();

        // Delete previous output
        FileSystem.get(conf).delete(new Path(output), true);

        // Job 1 - Find most used hashtag
        Job job_one = Job.getInstance(conf, "JSONTest");
        job_one.setJarByClass(JSONTest.class);

        job_one.setNumReduceTasks(1);

        job_one.setMapperClass(Map_One.class);
        job_one.setMapOutputKeyClass(Text.class);
        job_one.setMapOutputValueClass(IntWritable.class);

        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(IntWritable.class);

        job_one.setInputFormatClass(JSONInputFormat.class);
        job_one.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_one, new Path(input));
        FileOutputFormat.setOutputPath(job_one, new Path(output));

        job_one.waitForCompletion(true);

        return 0;

    }

    public static class Map_One extends Mapper<LongWritable, Text, Text, IntWritable> {

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            context.write(value, new IntWritable(1));

        }
    }

}
