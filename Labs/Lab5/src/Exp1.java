import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Exp1 extends Configured implements Tool {

    private static String input;
    private static String output;

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Requires 2 arguments arg0=inputPath and arg1=outputPath.  Only " + args.length + " were specified.");
            System.exit(-1);
        }

        input = args[0];
        output = args[1];

        int res = ToolRunner.run(new Configuration(), new Exp1(), args);

        System.exit(res);

    }

    @Override
    public int run(String[] args) throws Exception {

        // Create conf
        Configuration conf = new Configuration();

        // Delete previous output
        FileSystem.get(conf).delete(new Path(output), true);

        // Job 1 - Calculate number of times each hashtag is used
        Job job_one = Job.getInstance(conf, "Exp1 - Rd1");
        job_one.setJarByClass(Exp1.class);

        job_one.setMapperClass(Map_One.class);
        job_one.setMapOutputKeyClass(Text.class);
        job_one.setMapOutputValueClass(IntWritable.class);

        job_one.setReducerClass(Reduce_One.class);
        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(IntWritable.class);

        job_one.setInputFormatClass(JSONInputFormat.class);
        job_one.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_one, new Path(input));
        FileOutputFormat.setOutputPath(job_one, new Path(output));

        job_one.waitForCompletion(true);

        return 0;

    }

    public static class Map_One extends Mapper<LongWritable, Text, Text, IntWritable> {

        private JSONParser parser = new JSONParser();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            try {
                // Create JSONObject
                Object obj = parser.parse(value.toString());
                JSONObject jsonObj = (JSONObject) obj;

                // Get hashtag objects
                JSONObject entities = (JSONObject) jsonObj.get("entities");
                JSONArray hashtagsArr = (JSONArray) entities.get("hashtags");

                // Add hashtags to set (no duplicates)
                Set<String> hashtagSet = new HashSet<>();

                for (Object hashTagObj : hashtagsArr) {
                    JSONObject hashtag = (JSONObject) hashTagObj;
                    hashtagSet.add((String) hashtag.get("text"));
                }

                // Write hashtags in set to context
                for (String hashtag : hashtagSet) {
                    context.write(new Text(hashtag), new IntWritable(1));
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }

    public static class Reduce_One extends Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

            int sum = 0;

            // Add the 1's to get number of times used
            for (IntWritable val : values) {
                sum += val.get();
            }

            context.write(key, new IntWritable(sum));
        }
    }
}
