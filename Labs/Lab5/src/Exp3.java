import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Exp3 extends Configured implements Tool {

    private static String input;
    private static String output;

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Requires 2 arguments arg0=inputPath and arg1=outputPath.  Only " + args.length + " were specified.");
            System.exit(-1);
        }

        input = args[0];
        output = args[1];

        int res = ToolRunner.run(new Configuration(), new Exp3(), args);

        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {

        // Create conf
        Configuration conf = new Configuration();

        // Delete previous output
        FileSystem.get(conf).delete(new Path(output), true);

        // Job 1
        Job job_one = Job.getInstance(conf, "Exp3");
        job_one.setJarByClass(Exp3.class);

        job_one.setMapperClass(Map_One.class);
        job_one.setMapOutputKeyClass(Text.class);
        job_one.setMapOutputValueClass(Text.class);

        job_one.setReducerClass(Reduce_One.class);
        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(Text.class);

        job_one.setInputFormatClass(JSONInputFormat.class);
        job_one.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_one, new Path(input));
        FileOutputFormat.setOutputPath(job_one, new Path(output));

        job_one.waitForCompletion(true);

        return 0;
    }

    public static class Map_One extends Mapper<LongWritable, Text, Text, Text> {

        private JSONParser parser = new JSONParser();

        @Override
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            try {
                // Create JSONObject
                Object obj = parser.parse(value.toString());
                JSONObject jsonObj = (JSONObject) obj;

                // Get screen_name and followers_count from user object
                JSONObject user = (JSONObject) jsonObj.get("user");
                String screen_name = user.get("screen_name").toString();
                int statuses_count = Integer.parseInt(user.get("statuses_count").toString());

                // Get hashtags
                JSONObject entities = (JSONObject) jsonObj.get("entities");
                JSONArray hashtagsArr = (JSONArray) entities.get("hashtags");

                // Write hashtags to a single string
                String hashtagStr = "";

                for (Object hashTagObj : hashtagsArr) {
                    JSONObject hashtag = (JSONObject) hashTagObj;
                    hashtagStr += hashtag.get("text") + " ";
                }

                // Trim last char (trailing space) from string
                hashtagStr = hashtagStr.substring(0, hashtagStr.length() - 1);

                // Write to context
                context.write(new Text(screen_name), new Text("" + statuses_count + "\t" + hashtagStr));

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    public static class Reduce_One extends Reducer<Text, Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            int maxNumStatuses = 0;

            // Count number of times a hashtag is used by screen_name
            Map<String, Integer> hashtagCount = new HashMap<>();

            for (Text val : values) {

                // Convert to string
                String value = val.toString();

                // Get number of total status
                int numStatuses = Integer.parseInt(value.substring(0, value.indexOf('\t')));

                // Check if numStatuses is greater than numMaxStatuses
                if (numStatuses > maxNumStatuses) {
                    maxNumStatuses = numStatuses;
                }

                // Count how many times each hashtag occurs and place in hashmap
                for (String hashtag : value.substring(value.indexOf('\t')).split(" ")) {

                    if (hashtag.trim().length() == 0) {
                        continue; // Deal with empty hashtag
                    } else if (!hashtagCount.containsKey(hashtag)) {
                        hashtagCount.put(hashtag, 1);
                    } else {
                        hashtagCount.put(hashtag, hashtagCount.get(hashtag) + 1);
                    }

                }

            }

            // Determine most common hashtag and it's count
            String maxHashtagString = "";
            int maxHashtagCount = 0;

            for (String hashtag : hashtagCount.keySet()) {
                if (hashtagCount.get(hashtag) > maxHashtagCount) {
                    maxHashtagCount = hashtagCount.get(hashtag);
                    maxHashtagString = hashtag;
                }
            }

            // Write to context
            context.write(key, new Text("" + maxNumStatuses + " "+ maxHashtagString + " " + maxHashtagCount));

        }
    }

}
