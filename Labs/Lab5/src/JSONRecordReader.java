import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.LineReader;

import java.io.IOException;

public class JSONRecordReader extends RecordReader<LongWritable, Text> {

    private LineReader lineReader;

    private LongWritable key = new LongWritable();
    private Text value = new Text();

    private long start, end, position;

    //private int maxLineLength;

    @Override
    public void close() throws IOException {
        if (lineReader != null) {
            lineReader.close();
        }
    }

    @Override
    public LongWritable getCurrentKey() throws IOException, InterruptedException {
        return key;
    }

    @Override
    public Text getCurrentValue() throws IOException, InterruptedException {
        return value;
    }

    @Override
    public float getProgress() throws IOException, InterruptedException {
        if (start == end) {
            return 0.0f;
        } else {
            return Math.min(1.0f, (position - start) / (float) (end - start));
        }
    }

    @Override
    public void initialize(InputSplit genericSplit, TaskAttemptContext context) throws IOException, InterruptedException {

        Configuration conf = context.getConfiguration();

        // Retrieve file containing Split
        FileSplit split = (FileSplit) genericSplit;
        final Path file = split.getPath();
        FileSystem fs = file.getFileSystem(conf);

        // Set start and end
        start = split.getStart();
        end = start + split.getLength();
        position = start;


        // Open path and go to beginning
        FSDataInputStream fileIn = fs.open(split.getPath());
        fileIn.seek(start);

        lineReader = new LineReader(fileIn, conf);

    }

    @Override
    public boolean nextKeyValue() throws IOException, InterruptedException {

        // Key is current offset
        key.set(position);

        boolean jsonStarted = false;
        boolean jsonFinished = false;

        String jsonStr = "";

        while (position < end) {

            // Create Text for current line and clear
            Text curLineText = new Text();
            curLineText.clear();

            // Read line into currentLine
            int readLen = lineReader.readLine(curLineText);

            // Update position
            position += readLen;

            // Convert current line to string
            String curLineStr = curLineText.toString();

            // Check if start of JSON object
            if (curLineStr.startsWith("{")) {
                jsonStarted = true;
            }

            // Check if JSON object has started and add to JSON string
            if (jsonStarted) {
                if (curLineStr.startsWith("}")) {
                    jsonStr += "}";
                    jsonFinished = true;
                    break;
                } else {
                    jsonStr += curLineText.toString();
                }
            }

        }

        // Set value to the full JSON string
        if(jsonFinished) {
            value.set(jsonStr);
        }

        return jsonFinished;

    }

}
