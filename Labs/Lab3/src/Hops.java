import java.io.IOException;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class Hops {

    public static void main(String[] args) throws Exception {

        // Change following paths accordingly
        String input = "/class/s17419/lab3/patents.txt";
        String temp1 = "/scr/bfenton/lab3/exp1/temp1";
        String temp2 = "/scr/bfenton/lab3/exp1/temp2";
        String output = "/scr/bfenton/lab3/exp1/output/";

        // The number of reduce tasks
        int reduce_tasks = 6;

        Configuration conf = new Configuration();

        // Create job for round 1
        Job job_one = Job.getInstance(conf, "Hops rd1");

        // Attach the job to this Driver
        job_one.setJarByClass(Hops.class);

        // Fix the number of reduce tasks to run
        // If not provided, the system decides on its own
        job_one.setNumReduceTasks(reduce_tasks);

        // The datatype of the mapper output Key, Value
        job_one.setMapOutputKeyClass(Text.class);
        job_one.setMapOutputValueClass(Text.class);

        // The datatype of the reducer output Key, Value
        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(Text.class);

        // The class that provides the map method
        job_one.setMapperClass(Map_One.class);

        // The class that provides the reduce method
        job_one.setReducerClass(Reduce_One.class);

        // Decides how the input will be split
        // We are using TextInputFormat which splits the data line by line
        // This means each map method receives one line as an input
        job_one.setInputFormatClass(TextInputFormat.class);

        // Decides the Output Format
        job_one.setOutputFormatClass(TextOutputFormat.class);

        // The input HDFS path for this job
        // The path can be a directory containing several files
        // You can add multiple input paths including multiple directories
        FileInputFormat.addInputPath(job_one, new Path(input));

        // This is legal
        // FileInputFormat.addInputPath(job_one, new Path(another_input_path));

        // The output HDFS path for this job
        // The output path must be one and only one
        // This must not be shared with other running jobs in the system
        FileOutputFormat.setOutputPath(job_one, new Path(temp1));

        // This is not allowed
        // FileOutputFormat.setOutputPath(job_one, new Path(another_output_path));

        // Run the job
        job_one.waitForCompletion(true);


        // Create job for round 2
        // The output of the previous job can be passed as the input to the next
        // The steps are as in job 1

        Job job_two = Job.getInstance(conf, "Hops rd2");
        job_two.setJarByClass(Hops.class);
        job_two.setNumReduceTasks(reduce_tasks);

        // Should be match with the output datatype of mapper and reducer
        job_two.setMapOutputKeyClass(Text.class);
        job_two.setMapOutputValueClass(Text.class);

        job_two.setOutputKeyClass(Text.class);
        job_two.setOutputValueClass(Text.class);

        // If required the same Map / Reduce classes can also be used
        // Will depend on logic if separate Map / Reduce classes are needed
        // Here we show separate ones
        job_two.setMapperClass(Map_Two.class);
        job_two.setReducerClass(Reduce_One.class);  // REUSING FIRST REDUCER

        job_two.setInputFormatClass(TextInputFormat.class);
        job_two.setOutputFormatClass(TextOutputFormat.class);

        // The output of previous job set as input of the next
        FileInputFormat.addInputPath(job_two, new Path(temp1));
        FileOutputFormat.setOutputPath(job_two, new Path(temp2));

        // Run the job
        job_two.waitForCompletion(true);

        // Create job for round 3
        // The output of the previous job can be passed as the input to the next
        // The steps are as in job 1

        Job job_three = Job.getInstance(conf, "Hops rd3");
        job_three.setJarByClass(Hops.class);

        // Should be match with the output datatype of mapper and reducer
        job_three.setMapOutputKeyClass(Text.class);
        job_three.setMapOutputValueClass(Text.class);

        job_three.setOutputKeyClass(Text.class);
        job_three.setOutputValueClass(IntWritable.class);

        // If required the same Map / Reduce classes can also be used
        // Will depend on logic if separate Map / Reduce classes are needed
        // Here we show separate ones
        job_three.setMapperClass(Map_Three.class);
        job_three.setReducerClass(Reduce_Three.class);

        job_three.setInputFormatClass(TextInputFormat.class);
        job_three.setOutputFormatClass(TextOutputFormat.class);

        // The output of previous job set as input of the next
        FileInputFormat.addInputPath(job_three, new Path(temp2));
        FileOutputFormat.setOutputPath(job_three, new Path(output));

        // Run the job
        job_three.waitForCompletion(true);

    }


    // The Map Class
    // The input to the map method would be a LongWritable (long) key and Text
    // (String) value
    // Notice the class declaration is done with LongWritable key and Text value
    // The TextInputFormat splits the data line by line.
    // The key for TextInputFormat is nothing but the line number and hence can
    // be ignored
    // The value for the TextInputFormat is a line of text from the input
    // The map method can emit data using context.write() method
    public static class Map_One extends Mapper<LongWritable, Text, Text, Text> {

        // Create adjacency list
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String line[] = value.toString().split("\\s+");
            String from = line[0];
            String to = line[1];

            // use > and < to indicate downstream and upstream from the key, respectively
            context.write(new Text(from), new Text(">" + to));
            context.write(new Text(to), new Text("<" + from));
        }

    }


    // The Reduce class
    // The K, V values must match the output of the mapper class
    public static class Reduce_One extends Reducer<Text, Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            String adj = "";

            for (Text vertex : values) {
                context.progress();

                adj += vertex + ",";
            }

            // Remove trailing comma
            adj = adj.substring(0, adj.length() - 1);

            // Write value of keys in context
            context.write(key, new Text(adj));
        }
    }

    public static class Map_Two extends Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            // Get text strings and data
            String line[] = value.toString().split("\t");
            String middle = line[0];
            String values[] = line[1].split(",");

            // Find the set of upstream and downstream vertices
            Set<String> upstream = new HashSet<>();
            Set<String> downstream = new HashSet<>();

            for (String s : values) {
                if (s.charAt(0) == '<') {
                    upstream.add(s);
                } else {
                    downstream.add(s);
                }
            }

            // Build all possible edge sets of one and two hops
            for (String up : upstream) {
                for (String down : downstream) {
                    context.write(new Text(down.substring(1)), new Text(middle));
                    context.write(new Text(down.substring(1)), new Text(up.substring(1) + "-" + middle));
                    context.write(new Text(middle), new Text(up.substring(1)));
                }
            }

        }
    }


    // The third Map Class
    public static class Map_Three extends Mapper<LongWritable, Text, Text, Text> {

        Text t = new Text("key");

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String line[] = value.toString().split("\t");
            String values[] = line[1].split(",");

            Set<String> vals = new HashSet<>();

            for (String s : values) {
                // Split at vertex delimiter to count unique vertices, not just one-hops and two-hops separately
                String patents[] = s.split("-");
                Collections.addAll(vals, patents);
            }

            context.write(t, new Text(line[0] + "&" + vals.size()));

        }
    }

    // The third Reduce class
    public static class Reduce_Three extends Reducer<Text, Text, Text, IntWritable> {

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            // Count and track 10 highest values
            Map<String, Integer> topTen = new HashMap<>();

            for (Text t : values) {
                String str = t.toString();

                // Each line has format "0934-091348 \t 9001"
                String splitVals[] = str.split("&");
                int val = Integer.parseInt(splitVals[1]);
                String patent = splitVals[0];

                if (topTen.size() < 10) {
                    // First ten values are highest
                    topTen.put(patent, val);
                } else {
                    // Find the current top 10 min
                    Map.Entry<String, Integer> min = null;

                    for (Map.Entry<String, Integer> entry : topTen.entrySet()) {
                        if (min == null) {
                            // First value
                            min = entry;
                        } else {
                            if (entry.getValue() < min.getValue()) {
                                min = entry;
                            }
                        }
                    }

                    // Check if value is higher than min
                    if (min != null && val > min.getValue()) {
                        topTen.remove(min.getKey());
                        topTen.put(patent, val);
                    }
                }
            }

            // Write top ten values file
            for (Map.Entry<String, Integer> entry : topTen.entrySet()) {
                context.write(new Text(entry.getKey()), new IntWritable(entry.getValue()));
            }

        }
    }

}
