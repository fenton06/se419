import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class GCC {

    public static void main(String[] args) throws Exception {

        // Change following paths accordingly
        String input = "/class/s17419/lab3/patents.txt";
        String temp1 = "/scr/bfenton/lab3/exp2/temp1";
        String temp2 = "/scr/bfenton/lab3/exp2/temp2";
        String output = "/scr/bfenton/lab3/exp2/output/";

        // The number of reduce tasks
        int reduce_tasks = 70; // (1.95 * 2 * 18)

        Configuration conf = new Configuration();

        // Create job for round 1
        Job job_one = Job.getInstance(conf, "GCC Rd1");
        job_one.setJarByClass(GCC.class);
        job_one.setNumReduceTasks(reduce_tasks);

        job_one.setMapOutputKeyClass(IntWritable.class);
        job_one.setMapOutputValueClass(IntWritable.class);
        job_one.setMapperClass(Map_One.class);

        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(IntWritable.class);
        job_one.setReducerClass(Reduce_One.class);

        job_one.setInputFormatClass(TextInputFormat.class);
        job_one.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_one, new Path(input));
        FileOutputFormat.setOutputPath(job_one, new Path(temp1));

        job_one.waitForCompletion(true);


        // Create job for round 2
        Job job_two = Job.getInstance(conf, "GCC Rd2");
        job_two.setJarByClass(GCC.class);
        job_two.setNumReduceTasks(reduce_tasks);

        job_two.setMapperClass(Map_Two.class);
        job_two.setMapOutputKeyClass(Text.class);
        job_two.setMapOutputValueClass(Text.class);

        job_two.setReducerClass(Reduce_Two.class);
        job_two.setOutputKeyClass(Text.class);
        job_two.setOutputValueClass(Text.class);

        job_two.setInputFormatClass(TextInputFormat.class);
        job_two.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_two, new Path(temp1));
        FileOutputFormat.setOutputPath(job_two, new Path(temp2));

        job_two.waitForCompletion(true);

        // Create job for round 3
        Job job_three = Job.getInstance(conf, "GCC Rd3");
        job_three.setJarByClass(GCC.class);

        job_three.setMapperClass(Map_Two.class);
        job_three.setMapOutputKeyClass(Text.class);
        job_three.setMapOutputValueClass(Text.class);

        job_three.setReducerClass(Reduce_Three.class);
        job_three.setOutputKeyClass(Text.class);
        job_three.setOutputValueClass(Text.class);

        job_three.setInputFormatClass(TextInputFormat.class);
        job_three.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job_three, new Path(temp2));
        FileOutputFormat.setOutputPath(job_three, new Path(output));

        job_three.waitForCompletion(true);


    }

    public static class Map_One extends Mapper<LongWritable, Text, IntWritable, IntWritable> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            // Convert to an undirected graph by repeating each edge backwards
            StringTokenizer line = new StringTokenizer(value.toString());

            // Get vertex id and emit
            IntWritable from = new IntWritable(Integer.parseInt(line.nextToken()));
            IntWritable to = new IntWritable(Integer.parseInt(line.nextToken()));
            context.write(from, to);
            context.write(to, from);
        }

    }

    public static class Reduce_One extends Reducer<IntWritable, IntWritable, Text, IntWritable> {

        private IntWritable one = new IntWritable(1);

        public void reduce(IntWritable key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {

            // Build adj list
            Integer keyInt = key.get();

            List<Integer> list = new ArrayList<>();
            for (IntWritable i : values) {
                list.add(i.get());
            }

            Integer[] arr = list.toArray(new Integer[list.size()]);
            Arrays.sort(arr);

            // Emit all triplets
            for (int i = 0; i < arr.length - 1; i++)
                for (int j = i + 1; j < arr.length; j++) {
                    // Integer comparisons are much faster than building and sorting an array
                    if (keyInt < arr[i] && keyInt < arr[j]) { // A B C    A C B
                        if (arr[i] < arr[j])
                            context.write(new Text(keyInt.toString() + '-' + arr[i] + '-' + arr[j]), one);
                        else
                            context.write(new Text(keyInt.toString() + '-' + arr[j] + '-' + arr[i]), one);
                    } else if (arr[i] < arr[j]) { // B A C  B C A
                        if (keyInt < arr[j])
                            context.write(new Text(arr[i].toString() + '-' + keyInt + '-' + arr[j]), one);
                        else
                            context.write(new Text(arr[i].toString() + '-' + arr[j] + '-' + keyInt), one);
                    } else { // C A B    C B A
                        if (keyInt < arr[i])
                            context.write(new Text(arr[j].toString() + '-' + keyInt + '-' + arr[i]), one);
                        else
                            context.write(new Text(arr[j].toString() + '-' + arr[i] + '-' + keyInt), one);
                    }
                }

            context.progress();
        }
    }

    // The second Map Class
    public static class Map_Two extends Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            // Echo the data to feed into reducer
            StringTokenizer tokens = new StringTokenizer(value.toString());
            context.write(new Text(tokens.nextToken()), new Text(tokens.nextToken()));

        }
    }

    // The second Reduce class
    public static class Reduce_Two extends Reducer<Text, Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            int count = 0;

            // Count up the number of triplets
            for (Text ignored : values) {
                count++;
            }

            // Triplets,Triangles
            if (count == 3) {
                context.write(new Text("key"), new Text("3,1"));
            } else {
                context.write(new Text("key"), new Text("1,0"));
            }

            context.progress();
        }
    }


    // The third Reduce class
    public static class Reduce_Three extends Reducer<Text, Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            int numTriangles = 0;
            int numTriplets = 0;

            for (Text t : values) {
                // Separate triplets and triangles
                StringTokenizer tokens = new StringTokenizer(t.toString(),",");

                numTriplets += Integer.parseInt(tokens.nextToken());
                numTriangles += Integer.parseInt(tokens.nextToken());
            }

            double gcc = (3 * (double) numTriangles) / (double) numTriplets;

            context.write(new Text("GCC:"), new Text(String.format("%.3f", gcc)));

            context.progress();
        }

    }

}
