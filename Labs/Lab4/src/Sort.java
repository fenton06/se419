import java.io.IOException;
import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.InputSampler;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Sort extends Configured implements Tool {

    private static final String temp0 = "/scr/bfenton/lab4/temp0";
    private static String input;
    private static String output;

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Requires 2 arguments arg0=inputPath and arg1=outputPath.  Only " + args.length + " were specified.");
            System.exit(-1);
        }

        input = args[0];
        output = args[1];

        int res = ToolRunner.run(new Configuration(), new Sort(), args);

        System.exit(res);

    }

    public int run(String[] args) throws Exception {

        // Create conf
        Configuration conf = new Configuration();

        // Delete files
        FileSystem.get(conf).delete(new Path(temp0), true);
        FileSystem.get(conf).delete(new Path(output), true);

        /*
         * Job 1 - Convert from text to sequence
         */
        Job job_one = Job.getInstance(conf, "Convert to Sequence");
        job_one.setJarByClass(Sort.class);

        // Input and output files
        TextInputFormat.addInputPath(job_one, new Path(input));
        SequenceFileOutputFormat.setOutputPath(job_one, new Path(temp0));

        // Output file format
        job_one.setOutputFormatClass(SequenceFileOutputFormat.class);

        // Set mapper
        job_one.setMapperClass(Map_One.class);
        job_one.setMapOutputKeyClass(Text.class);
        job_one.setMapOutputValueClass(Text.class);

        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(Text.class);

        // No reduce tasks -> directly to HDFS
        job_one.setNumReduceTasks(0);

        // submit and wait for completion
        job_one.waitForCompletion(true);


        /*
         * Job 2 - Sort!
         */
        Job job_two = Job.getInstance(conf, "Sort");

        job_two.setJarByClass(Sort.class);

        FileInputFormat.addInputPath(job_two, new Path(temp0));
        FileOutputFormat.setOutputPath(job_two, new Path(output));

        job_two.setInputFormatClass(SequenceFileInputFormat.class);
        job_two.setOutputFormatClass(TextOutputFormat.class);

        job_two.setMapperClass(Mapper.class);

        job_two.setReducerClass(Reduce_Two.class);
        job_two.setOutputKeyClass(Text.class);
        job_two.setOutputValueClass(Text.class);

        job_two.setNumReduceTasks(8);

        // Set partitioner class
        job_two.setPartitionerClass(TotalOrderPartitioner.class);

        // Create Sampler and write Partition to conf
        InputSampler.Sampler<Text, Text> sampler = new InputSampler.RandomSampler<>(0.1, 10000, 8);
        InputSampler.writePartitionFile(job_two, sampler);

        // Add partition file to job
        String partitionFile = TotalOrderPartitioner.getPartitionFile(conf);
        URI partitionUri = new URI(partitionFile);
        job_two.addCacheFile(partitionUri);

        // Set partition file
        //TotalOrderPartitioner.setPartitionFile(job_two.getConfiguration(), new Path("/class/s17419/lab4/partition"));  // Given partition

        job_two.waitForCompletion(true);

        // Delete temp folders
        FileSystem.get(conf).delete(new Path(temp0), true);

        return 0;

    }

    // Convert to serialize
    public static class Map_One extends Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            context.write(new Text(value.toString().substring(0, 10)), new Text((value.toString().substring(12))));

        }

    }

    public static class Reduce_Two extends Reducer<Text, Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            for(Text t: values) {
                context.write(key, t);
            }

        }

    }

}
