-- Load files
ipTraceLines = LOAD '/class/s17419/lab6/ip_trace' USING PigStorage(' ') AS (time:chararray, id:long, src_ip:chararray, bracket:chararray, dest_ip:chararray, protocol:chararray, protoDepData:chararray);
rawBlockLines = LOAD '/class/s17419/lab6/raw_block' USING PigStorage(' ') AS (id:long, status:chararray);

-- Keep only blocked statuses from rawBlockLines
blocked = FILTER rawBlockLines BY status == 'Blocked';

-- Intersect
joined = JOIN ipTraceLines BY id, blocked BY id;

-- Generate columns we want to keep
firewall = FOREACH joined GENERATE time, ipTraceLines::id AS id, src_ip, dest_ip, status;

-- Output firewall
STORE firewall INTO '/scr/bfenton/lab6/exp3/firewall' USING PigStorage(' ');

-- Group and count
groups = GROUP firewall BY src_ip;
totals = FOREACH groups GENERATE group, COUNT(firewall) AS count;

-- Sort and store
ordered = ORDER totals BY count DESC;
STORE ordered INTO '/scr/bfenton/lab6/exp3/output' USING PigStorage('\t');