-- Load file
info = LOAD '/class/s17419/lab6/network_trace' USING PigStorage(' ') AS (time:chararray, IP:chararray, src_ip:chararray, bracket:chararray, dest_ip:chararray, protocol:chararray, protoDepData:chararray);

-- Keep tcp records only
filtered = FILTER info BY protocol == 'tcp';

-- Remove unnessessary IP information
pairs = FOREACH filtered GENERATE SUBSTRING(src_ip, 0, LAST_INDEX_OF(src_ip, '.')) AS src_ip, SUBSTRING(dest_ip, 0, LAST_INDEX_OF(dest_ip, '.')) AS dest_ip;

-- Remove duplicates
distinctPairs = DISTINCT pairs;

-- Group by src ip and count dest ips
sources = GROUP distinctPairs BY src_ip;
totals = FOREACH sources GENERATE group, COUNT(distinctPairs) AS count;

-- Sort, limit, and store
ordered = ORDER totals BY count DESC;
top = LIMIT ordered 10;
STORE top INTO '/scr/bfenton/lab6/exp2/output' USING PigStorage('\t');
