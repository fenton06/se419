-- Load file
info = LOAD 'gaz_tracts_national.txt' USING PigStorage('\t') AS (usps:chararray, geoid:chararray, pop10:int, hu10:int, aland:long, awater:int, alandsqmi:int, awatersqmi:int, intptlat:int, intptlong:int);

-- Combine based on state and sum
states = GROUP info BY usps;
totals = FOREACH states GENERATE group, SUM(info.aland) AS sum;

-- Order -> split -> output
ordered = ORDER totals BY sum DESC;
top = LIMIT ordered 10;
STORE top INTO 'Exp1Output' USING PigStorage('\t');