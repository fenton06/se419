/*
 *****************************************
 *****************************************
 * Cpr E 419 - Lab 2 *********************
 * For question regarding this code,
 * please ask on Piazza
 *****************************************
 *****************************************
 */

import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class Driver {

    public static void main(String[] args) throws Exception {

        // Change following paths accordingly
        String input = "/class/s17419/lab2/gutenberg";
        String temp = "/scr/bfenton/lab2/exp2/temp";
        String output = "/scr/bfenton/lab2/exp2/output/";

        // The number of reduce tasks
        int reduce_tasks = 4;

        Configuration conf = new Configuration();

        // Create job for round 1
        Job job_one = Job.getInstance(conf, "Driver Program Round One");

        // Attach the job to this Driver
        job_one.setJarByClass(Driver.class);

        // Fix the number of reduce tasks to run
        // If not provided, the system decides on its own
        job_one.setNumReduceTasks(reduce_tasks);


        // The datatype of the mapper output Key, Value
        job_one.setMapOutputKeyClass(Text.class);
        job_one.setMapOutputValueClass(IntWritable.class);

        // The datatype of the reducer output Key, Value
        job_one.setOutputKeyClass(Text.class);
        job_one.setOutputValueClass(IntWritable.class);

        // The class that provides the map method
        job_one.setMapperClass(Map_One.class);

        // The class that provides the reduce method
        job_one.setReducerClass(Reduce_One.class);

        // Decides how the input will be split
        // We are using TextInputFormat which splits the data line by line
        // This means each map method receives one line as an input
        job_one.setInputFormatClass(TextInputFormat.class);

        // Decides the Output Format
        job_one.setOutputFormatClass(TextOutputFormat.class);

        // The input HDFS path for this job
        // The path can be a directory containing several files
        // You can add multiple input paths including multiple directories
        FileInputFormat.addInputPath(job_one, new Path(input));

        // This is legal
        // FileInputFormat.addInputPath(job_one, new Path(another_input_path));

        // The output HDFS path for this job
        // The output path must be one and only one
        // This must not be shared with other running jobs in the system
        FileOutputFormat.setOutputPath(job_one, new Path(temp));

        // This is not allowed
        // FileOutputFormat.setOutputPath(job_one, new Path(another_output_path));

        // Run the job
        job_one.waitForCompletion(true);



        // Create job for round 2
        // The output of the previous job can be passed as the input to the next
        // The steps are as in job 1

        Job job_two = Job.getInstance(conf, "Driver Program Round Two");
        job_two.setJarByClass(Driver.class);
        //job_two.setNumReduceTasks(reduce_tasks);

        // Should be match with the output datatype of mapper and reducer
        job_two.setMapOutputKeyClass(Text.class);
        job_two.setMapOutputValueClass(Text.class);

        job_two.setOutputKeyClass(Text.class);
        job_two.setOutputValueClass(IntWritable.class);

        // If required the same Map / Reduce classes can also be used
        // Will depend on logic if separate Map / Reduce classes are needed
        // Here we show separate ones
        job_two.setMapperClass(Map_Two.class);
        job_two.setReducerClass(Reduce_Two.class);

        job_two.setInputFormatClass(TextInputFormat.class);
        job_two.setOutputFormatClass(TextOutputFormat.class);

        // The output of previous job set as input of the next
        FileInputFormat.addInputPath(job_two, new Path(temp));
        FileOutputFormat.setOutputPath(job_two, new Path(output));

        // Run the job
        job_two.waitForCompletion(true);

    }


    // The Map Class
    // The input to the map method would be a LongWritable (long) key and Text
    // (String) value
    // Notice the class declaration is done with LongWritable key and Text value
    // The TextInputFormat splits the data line by line.
    // The key for TextInputFormat is nothing but the line number and hence can
    // be ignored
    // The value for the TextInputFormat is a line of text from the input
    // The map method can emit data using context.write() method
    // However, to match the class declaration, it must emit Text as key and
    // IntWribale as value
    public static class Map_One extends Mapper<LongWritable, Text, Text, IntWritable> {

        private IntWritable one = new IntWritable(1);
        private Text bigram = new Text();

        // The map method
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            // The TextInputFormat splits the data line by line.
            // So each map method receives one line from the input
            String line = value.toString();
            String prev = null;

            /*
             * Replace ' and - with ""
             * Replace all whitespace and punctuation (except for sentence ending chars) with a single space
             * Convert to lowercase
             * Remove leading/trailing whitespace
             * Split line into words at single space
             */

            line = line.replaceAll("['-]+", "").replaceAll("[[\\p{Punct}&&[^.?!]]\\p{Space}]+", " ").toLowerCase().trim();

            StringTokenizer tokens = new StringTokenizer(line);

            while(tokens.hasMoreTokens()) {
                context.progress();

                String word = tokens.nextToken();

                // Create bigram
                if (prev != null) {
                    // Emit after removing punctuation and converting to lowercase
                    bigram.set((prev + " " + word).replaceAll("\\p{Punct}", "").toLowerCase());
                    context.write(bigram, one);
                }

                // Check if end of sentence
                if (!word.matches(".*[.?!]")) {
                    prev = word;
                } else {
                    prev = null;
                }

            }

        }
    }


    // The Reduce class
    // The key is Text and must match the datatype of the output key of the map
    // method
    // The value is IntWritable and also must match the datatype of the output
    // value of the map method
    public static class Reduce_One extends Reducer<Text, IntWritable, Text, IntWritable> {

        // The reduce method
        // For key, we have an Iterable over all values associated with this key
        // The values come in a sorted fasion.
        public void reduce(Text key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {

            // Sum of values
            int sum = 0;

            // Get sum
            for (IntWritable val : values) {
                context.progress();
                sum += val.get();
            }

            // Write value of keys in context
            context.write(key, new IntWritable(sum));
        }
    }

    // The second Map Class
    public static class Map_Two extends Mapper<LongWritable, Text, Text, Text> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            // Get text string
            String line = value.toString();

            // Mare sure it's a letter not a digit
            if (line.substring(0,1).matches("[a-z]")) {
                // Write startLetter as key, and digram/count as value
                context.write(new Text(line.substring(0, 1)), new Text(line));
            }

        }
    }

    // The second Reduce class
    public static class Reduce_Two extends Reducer<Text, Text, Text, IntWritable> {

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

            String mostUsed = "";
            int mostUsedCount = 0;

            // Go through each letter and find most frequently used bigram starting with that letter
            for (Text digramEntry : values) {
                context.progress();

                String digramStr = digramEntry.toString();

                // Use tab delimiter to separate digram from number of times used
                Scanner parser = new Scanner(digramStr).useDelimiter("\\t");

                String tempDigram = parser.next();
                int tempCount = parser.nextInt();

                // Check if tempcount is higher than current max
                if (tempCount > mostUsedCount) {
                    mostUsed = tempDigram;
                    mostUsedCount = tempCount;
                }

            }

            // Emit the most used digram and count
            context.write(new Text(mostUsed), new IntWritable(mostUsedCount));

        }
    }

}
