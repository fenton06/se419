import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class Firewall {

    //private final static int numOfReducers = 2;

    //@SuppressWarnings("serial")
    public static void main(String[] args) throws Exception {

        // Error check
        if (args.length != 4) {
            System.err.println("Usage: Firewall <ip_trace> <raw_blocks> <firewall> <blocked_IPs>");
            System.exit(1);
        }

        // Spark setup
        SparkConf sparkConf = new SparkConf().setAppName("Firewall in Spark");
        JavaSparkContext context = new JavaSparkContext(sparkConf);

        // Open files
        JavaRDD<String> ipTraceLines = context.textFile(args[0]);
        JavaRDD<String> rawBlockLines = context.textFile(args[1]);

        /*
         * Convert files to key/value pairs
         */

        // ipTrace conversion
        JavaPairRDD<String, String> ipTraces = ipTraceLines.mapToPair(
            new PairFunction<String, String, String>() {
                @Override
                public Tuple2<String, String> call(String s) throws Exception {
                    // Split by spaces
                    String[] tokens = s.split("\\s+");

                    // Assign values
                    String time = tokens[0];
                    String id = tokens[1];
                    String src_ip = tokens[2];
                    String dest_ip = tokens[4];

                    // Assign values (key = id)
                    String value = time + " " + src_ip + " " + dest_ip;
                    return new Tuple2<>(id, value);
                }
            }
        ).cache();

        // rawBlock conversion
        JavaPairRDD<String, String> rawBlocksUnfiltered = rawBlockLines.mapToPair(
            new PairFunction<String, String, String>() {
                @Override
                public Tuple2<String, String> call(String s) throws Exception {
                    // Split by spaces
                    String[] tokens = s.split("\\s+");

                    // Assign values (id = tokens[0] status = tokens[1])
                    return new Tuple2<>(tokens[0], tokens[1]);
                }
            }
        ).cache();

        // Filter rawblock
        JavaPairRDD<String, String> rawBlocks = rawBlocksUnfiltered.filter(
            new Function<Tuple2<String, String>, Boolean>() {
                @Override
                public Boolean call(Tuple2<String, String> tuple) throws Exception {
                    String status = tuple._2;
                    return status.equals("Blocked");
                }
            }
        );

        // Intersect RDD's
        JavaPairRDD<String, Tuple2<String, String>> joinedMess = ipTraces.join(rawBlocks);

        //Write firewall as a string and not K,V pair
        JavaRDD<String> firewall = joinedMess.map(
            new Function<Tuple2<String, Tuple2<String, String>>, String>() {
                @Override
                public String call(Tuple2<String, Tuple2<String, String>> tuple) throws Exception {

                    // Get info
                    String id = tuple._1;
                    String info = tuple._2._1;
                    String status = tuple._2._2;

                    // Split info (time = [0], src_ip = [1], dst_ip = [2]
                    String[] infoArr = info.split("\\s+");

                    // <Time> <Connection ID> <Source IP> <Destination IP> "Blocked"
                    return infoArr[0] + " " + id + " " + infoArr[1] + " " + infoArr[2] + " " + status;
                }
            }
        );

        // Write firewall
        firewall.saveAsTextFile(args[2]);

        // Find block counts
        JavaPairRDD<String, Integer> ipOnes = firewall.mapToPair(
            new PairFunction<String, String, Integer>() {
                @Override
                public Tuple2<String, Integer> call(String line) {
                    String[] firewallLineArr = line.split("\\s+");
                    return new Tuple2<>(firewallLineArr[2], 1);
                }
            }
        );

        // Sum counts
        JavaPairRDD<String, Integer> blockedCount = ipOnes.reduceByKey(
            new Function2<Integer, Integer, Integer>() {
                @Override
                public Integer call(Integer i1, Integer i2) {
                    return i1 + i2;
                }
            }
        );

        //Reverse to sort
        JavaPairRDD<Integer, String> reversedCounts = blockedCount.mapToPair(
            new PairFunction<Tuple2<String, Integer>, Integer, String>() {
                @Override
                public Tuple2<Integer, String> call(Tuple2<String, Integer> myTuple) throws Exception {
                    return new Tuple2<>(myTuple._2, myTuple._1);
                }
            }
        );

        // Sort
        JavaPairRDD<Integer, String> sorted = reversedCounts.sortByKey(false, 1);

        // Reverse back and write to string
        JavaRDD<String> sortedBlocked = sorted.map(
            new Function<Tuple2<Integer, String>, String>() {
                @Override
                public String call(Tuple2<Integer, String> tuple) throws Exception {
                    return tuple._2 + " " + tuple._1;
                }
            }
        );

        // Write blocked counts
        sortedBlocked.saveAsTextFile(args[3]);

        context.stop();
        context.close();

    }

}
