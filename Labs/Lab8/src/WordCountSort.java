import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class WordCountSort {

    private final static int numOfReducers = 2;

    @SuppressWarnings("serial")
    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.err.println("Usage: WordCountSort <input> <output>");
            System.exit(1);
        }

        SparkConf sparkConf = new SparkConf().setAppName("WordCountSort in Spark");
        JavaSparkContext context = new JavaSparkContext(sparkConf);
        JavaRDD<String> lines = context.textFile(args[0]);

        // Split lines into individual words
        JavaRDD<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
            @Override
            public Iterable<String> call(String s) {
                return Arrays.asList(s.split("\\s+"));
            }
        });

        // Assign a 1 to each word
        JavaPairRDD<String, Integer> ones = words.mapToPair(new PairFunction<String, String, Integer>() {
            @Override
            public Tuple2<String, Integer> call(String s) {
                return new Tuple2<>(s, 1);
            }
        });

        // Add 1's to find word count
        JavaPairRDD<String, Integer> counts = ones.reduceByKey(new Function2<Integer, Integer, Integer>() {
            @Override
            public Integer call(Integer i1, Integer i2) {
                return i1 + i2;
            }
        }, numOfReducers);

        JavaPairRDD<Integer, String> reversedCounts = counts.mapToPair(new PairFunction<Tuple2<String, Integer>, Integer, String>() {
            @Override
            public Tuple2<Integer, String> call(Tuple2<String, Integer> myTuple) throws Exception {
                return new Tuple2<>(myTuple._2, myTuple._1);
            }
        });

        JavaPairRDD<Integer, String> sorted = reversedCounts.sortByKey(false, 1);

        sorted.saveAsTextFile(args[1]);
        context.stop();
        context.close();

    }
}