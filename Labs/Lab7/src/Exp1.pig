REGISTER /home/bfenton/lab7/MyUDFs.jar;

-- Jan 1, 1990 to Jan 3, 2000
DEFINE Exp1_1 MyUDFs.ExpOne('19900101','20000103');

-- Jan 2, 2005 to Jan 31, 2014
DEFINE Exp1_2 MyUDFs.ExpOne('20050102','20140131');

-- Load data
data = LOAD '/class/s17419/lab7/historicaldata.csv' USING PigStorage(',') AS (ticker:chararray, date:int, open:float, high:float, low:float, close:float, volume:int);

-- Remove garbage data (if any)
goodData = FILTER data BY (date IS NOT NULL AND open IS NOT NULL);

-- Prune extra (unneccessary) info
lessData = FOREACH goodData GENERATE ticker, date, open;

-- Group data by ticker
companyData = GROUP lessData BY ticker;


-- Find growth factor of first data set using UDF
growthFactor1 = FOREACH companyData {
    GENERATE group, Exp1_1(lessData) AS growth;
}

-- Sort, find top company, and write output
sortedGrowth1 = ORDER growthFactor1 BY growth DESC;
top1 = LIMIT sortedGrowth1 1;
STORE top1 INTO '/scr/bfenton/lab7/exp1/output1';


-- Find growth factor of second data set using UDF
growthFactor2 = FOREACH companyData {
    GENERATE group, Exp1_2(lessData) AS growth;
}

-- Sort, find top company, and write output
sortedGrowth2 = ORDER growthFactor2 BY growth DESC;
top2 = LIMIT sortedGrowth2 1;
STORE top2 INTO '/scr/bfenton/lab7/exp1/output2';
