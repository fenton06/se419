package MyUDFs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.DataBag;
import org.apache.pig.data.TupleFactory;

public class ExpTwo extends EvalFunc<Tuple> {

    private int start;
    private int end;

    public ExpTwo(String start, String end) {
        this.start = Integer.parseInt(start);
        this.end = Integer.parseInt(end);
    }

    public ExpTwo() {

    }

    @Override
    public Tuple exec(Tuple input) throws IOException {

        // Null/size checks
        if (input == null || input.size() == 0 || input.get(0) == null) {
            return null;
        }

        // Get object and convert to DataBag, Tuple is in form {ticker, date, open}
        DataBag days = (DataBag) input.get(0);

        // Put all tuples into Stock objects
        List<Stock> stocks = new ArrayList<>();

        for (Tuple day : days) {
            Stock tempStock = new Stock((String) day.get(0), (int) day.get(1), (float) day.get(2));
            stocks.add(tempStock);
        }

        // Sort the data by date (using overridden compareTo)
        Collections.sort(stocks);

        // Move i to desired start date
        int i = 0;
        while (i < stocks.size() && stocks.get(i).date < start) {
            i++;
        }

        // Find moving averages and append to big tuple
        TupleFactory tf = TupleFactory.getInstance();
        Tuple t = tf.newTuple();

        for (; i < stocks.size(); i++) {
            // Get current stock data
            Stock cur = stocks.get(i);

            // Stop at end date
            if(cur.date > end) {
                break;
            }

            // Get moving average
            cur.movingAvg = getMovingAvg(stocks, i);

            // Append to tuple
            t.append(cur.asTuple());
        }

        return t;
    }

    private float getMovingAvg(List<Stock> data, int targetIndex) {

        // Initialize vars
        float tot = 0.0f;
        int numItems = 0;

        // Compute sum of opening prices
        for (int i = targetIndex - 1; i >= 0 && numItems < 20; i--) {
            tot += data.get(i).openPrice;
            numItems++;
        }

        // Return average
        return tot / numItems;
    }

    private class Stock implements Comparable<Stock> {

        final String ticker;
        final int date;
        final float openPrice;
        float movingAvg;

        Stock(String t, int d, float o) {
            ticker = t;
            date = d;
            openPrice = o;
        }

        @Override
        public int compareTo(Stock o) {
            return (this.date - o.date);
        }

        Tuple asTuple() {

            Tuple t = TupleFactory.getInstance().newTuple();

            t.append(ticker);
            t.append(date);
            t.append(openPrice);
            t.append(movingAvg);

            return t;
        }
    }

}
