package MyUDFs;

import java.io.IOException;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.DataBag;

public class ExpOne extends EvalFunc<Float> {

    private int start;
    private int end;

    public ExpOne(String start, String end) {
        this.start = Integer.parseInt(start);
        this.end = Integer.parseInt(end);
    }

    public ExpOne() {

    }

    @Override
    public Float exec(Tuple input) throws IOException {

        // Null/size checks
        if (input == null || input.size() == 0 || input.get(0) == null) {
            return null;
        }

        // Get object and convert to DataBag, Tuple is in form {ticker, date, open}
        DataBag days = (DataBag) input.get(0);

        //Tuple day;
        Tuple firstDay = null;
        Tuple lastDay = null;

        // Starting first and last days
        int first = 20200000;
        int last = 18000000;

        for (Tuple day : days) {
            // Get date
            int date = (Integer) day.get(1);

            // Find first day
            if (date < first && date >= start) {
                first = date;
                firstDay = day;
            }

            // Find last day
            if (date > last && date <= end) {
                last = date;
                lastDay = day;
            }

            // Correct days have been found (speed things up!)
            if (first == start && last == end) {
                break;
            }

        }

        // No nulls allowed!
        if(firstDay != null && lastDay !=null) {
            return ((Float) lastDay.get(2) / (Float) firstDay.get(2));
        } else {
            return null;
        }

    }

}
