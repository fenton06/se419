REGISTER /home/bfenton/lab7/MyUDFs.jar;

-- Oct 1, 2013 to Oct 31, 2013
DEFINE Exp2 MyUDFs.ExpTwo('20131001','20131031');

-- Load data
data = LOAD '/class/s17419/lab7/historicaldata.csv' USING PigStorage(',') AS (ticker:chararray, date:int, open:float, high:float, low:float, close:float, volume:int);

-- Remove garbage data (if any)
goodData = FILTER data BY (date IS NOT NULL AND open IS NOT NULL);

-- Prune extra (unneccessary) info
lessData = FOREACH goodData GENERATE ticker, date, open;

-- Group data by ticker
companyData = GROUP lessData BY ticker;

-- Get moving average for each stock (returns complete tuple)
movingAverages = FOREACH companyData GENERATE Exp2(lessData);

-- Write output
STORE movingAverages INTO '/scr/bfenton/lab7/exp2/output';