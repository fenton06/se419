import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;

import scala.Tuple2;

import java.util.*;

public class GraphCycles {

    public static void main(String[] args) throws Exception {

        // Error check
        if (args.length != 2) {
            System.err.println("Usage: GraphCycles <input> <output>");
            System.exit(1);
        }

        SparkConf sparkConf = new SparkConf().setAppName("Directed Cycle Counter");
        JavaSparkContext context = new JavaSparkContext(sparkConf);

        // Open graph file
        JavaRDD<String> rawData = context.textFile(args[0]);

        // Build RDD of vertex outgoing edges
        JavaPairRDD<String, String> edgesOut = rawData.mapToPair(
                new PairFunction<String, String, String>() {
                    @Override
                    public Tuple2<String, String> call(String s) throws Exception {
                        // Split by space
                        String[] tokens = s.split("\\s+");

                        // from = tokens[0], to = tokens[1] (from > to)
                        return new Tuple2<>(tokens[0], tokens[1]);
                    }
                }
        );

        // Build RDD of vertex incoming edges
        JavaPairRDD<String, String> edgesIn = rawData.mapToPair(
                new PairFunction<String, String, String>() {
                    @Override
                    public Tuple2<String, String> call(String s) throws Exception {
                        // Split by space
                        String[] tokens = s.split("\\s+");

                        // from = tokens[0], to = tokens[1] (to < from)
                        return new Tuple2<>(tokens[1], tokens[0]);
                    }
                }
        );

        // 16 partitions for 16 workers
        JavaPairRDD<String, Iterable<String>> edgesOutList = edgesOut.groupByKey(16);
        JavaPairRDD<String, Iterable<String>> edgesInList = edgesIn.groupByKey(16);

        // Join RDD's for complete vertex in/out info
        JavaPairRDD<String, Tuple2<Iterable<String>, Iterable<String>>> adjList = edgesOutList.join(edgesInList);

        // Generate all possible truples and reorder from low to high
        JavaRDD<String> triples = adjList.flatMap(
                new FlatMapFunction<Tuple2<String, Tuple2<Iterable<String>, Iterable<String>>>, String>() {
                    @Override
                    public Iterable<String> call(Tuple2<String, Tuple2<Iterable<String>, Iterable<String>>> tup) throws Exception {

                        String middleStr = tup._1;

                        Set<String> out = new HashSet<>();
                        Set<String> in = new HashSet<>();

                        // Add all out edges to set
                        for (String s : tup._2._1) {
                            out.add(s);
                        }

                        // Add all in edges to set
                        for (String s : tup._2._2) {
                            in.add(s);
                        }

                        List<String> triplets = new ArrayList<>();

                        // Build all triplets
                        for (String fromStr : in) {
                            for (String toStr : out) {

                                int from = Integer.parseInt(fromStr);
                                int middle = Integer.parseInt(middleStr);
                                int to = Integer.parseInt(toStr);

                                // Reorder
                                if (from < middle && from < to) {
                                    if(middle < to) {
                                        triplets.add(fromStr + " " + middleStr + " " + toStr);
                                    } else {
                                        triplets.add(fromStr + " " + toStr + " " + middleStr);
                                    }
                                } else if (middle < from && middle < to) {
                                    if(to < from) {
                                        triplets.add(middleStr + " " + toStr + " " + fromStr);
                                    } else {
                                        triplets.add(middleStr + " " + fromStr + " " + toStr);
                                    }
                                } else {
                                    if(from < middle) {
                                        triplets.add(toStr + " " + fromStr + " " + middleStr);
                                    } else {
                                        triplets.add(toStr + " " + middleStr + " " + fromStr);
                                    }
                                }
                            }
                        }

                        return triplets;
                    }
                }
        );

        // Add ones for summing
        JavaPairRDD<String, Integer> ones = triples.mapToPair(
                new PairFunction<String, String, Integer>() {
                    @Override
                    public Tuple2<String, Integer> call(String s) throws Exception {
                        return new Tuple2<>(s, 1);
                    }
                }
        );

        // Add 1's to see if we have 3 triplets that are identical
        JavaPairRDD<String, Integer> counts = ones.reduceByKey(
                new Function2<Integer, Integer, Integer>() {
                    @Override
                    public Integer call(Integer i1, Integer i2) {
                        return i1 + i2;
                    }
                }
        );

        // Reverse tuples to filter for 3 (triangle = cycle)
        JavaPairRDD<Integer, String> revCounts = counts.mapToPair(
                new PairFunction<Tuple2<String, Integer>, Integer, String>() {
                    @Override
                    public Tuple2<Integer, String> call(Tuple2<String, Integer> tup) throws Exception {
                        return new Tuple2<>(tup._2, tup._1);
                    }
                }
        );

        // Filter counts to 3
        JavaPairRDD<Integer, String> filteredCounts = revCounts.filter(
                new Function<Tuple2<Integer, String>, Boolean>() {
                    @Override
                    public Boolean call(Tuple2<Integer, String> tup) throws Exception {
                        return tup._1 == 3;
                    }
                }
        );

        // Generate answer string
        Long cycleCount = filteredCounts.count();
        String ansStr = "Directed cycles of length 3: " + cycleCount.toString();

        // Write answer to file
        JavaRDD<String> ans = context.parallelize(Collections.singletonList(ansStr), 1);
        ans.saveAsTextFile(args[1]);

        context.stop();
        context.close();

    }

}
