import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class Github {

    public static void main(String[] args) throws Exception {

        SparkConf sparkConf = new SparkConf().setAppName("Github - Language Count and Repo Stars");
        JavaSparkContext context = new JavaSparkContext(sparkConf);

        // Open github file
        JavaRDD<String> rawData = context.textFile("/class/s17419/lab9/github.csv");

        // Put info in RDD to find max repo stars
        // Formatted as <Language>, Tuple2< <Repo Name>, <Star Count> >
        JavaPairRDD<String, Tuple2<String, Integer>> data = rawData.mapToPair(
                new PairFunction<String, String, Tuple2<String, Integer>>() {
                    @Override
                    public Tuple2<String, Tuple2<String, Integer>> call(String s) throws Exception {
                        // Split by comma
                        String[] tokens = s.split(",");

                        // Assign values
                        String name = tokens[0];
                        String language = tokens[1];
                        int stars = Integer.parseInt(tokens[12]);

                        // <Language>, < <Repo Name>, <Star Count> >
                        return new Tuple2<>(language, new Tuple2<>(name, stars));
                    }
                }
        );

        // Reduce each language to most starred repo and name
        JavaPairRDD<String, Tuple2<String, Integer>> maxStars = data.reduceByKey(
                new Function2<Tuple2<String, Integer>, Tuple2<String, Integer>, Tuple2<String, Integer>>() {
                    @Override
                    public Tuple2<String, Integer> call(Tuple2<String, Integer> tup1, Tuple2<String, Integer> tup2) throws Exception {
                        if (tup1._2 > tup2._2) {
                            return tup1;
                        } else {
                            return tup2;
                        }
                    }
                }
        );

        // Assign a 1 to each language and line
        // Tuple2<Language, 1>
        JavaPairRDD<String, Integer> ones = rawData.mapToPair(
                new PairFunction<String, String, Integer>() {
                    @Override
                    public Tuple2<String, Integer> call(String s) throws Exception {

                        // Split by comma
                        String[] tokens = s.split(",");

                        // Assign values
                        String language = tokens[1];

                        return new Tuple2<>(language, 1);
                    }
                }
        );

        // Add 1's to get how many times a language is used
        JavaPairRDD<String, Integer> count = ones.reduceByKey(
                new Function2<Integer, Integer, Integer>() {
                    @Override
                    public Integer call(Integer i1, Integer i2) throws Exception {
                        return i1 + i2;
                    }
                }
        );

        // Join the two RDD's based on language
        // Will have form Tuple2< <Language>, Tuple2<Count, Tuple2<Name, Stars> > >
        JavaPairRDD<String, Tuple2<Integer, Tuple2<String, Integer> > > joined = count.join(maxStars);

        // Clean up info and make language count key for sorting
        JavaPairRDD<Integer, String> countKey = joined.mapToPair(
                new PairFunction<Tuple2<String, Tuple2<Integer, Tuple2<String, Integer>>>, Integer, String>() {
                    @Override
                    public Tuple2<Integer, String> call(Tuple2<String, Tuple2<Integer, Tuple2<String, Integer>>> tup) throws Exception {

                        String language = tup._1;
                        int count = tup._2._1;
                        String repoName = tup._2._2._1;
                        int stars = tup._2._2._2;

                        String value = language + "," + repoName + "," + stars;

                        // Key = count (for sorting next) and value is a string of info (language,repoName,stars)
                        return new Tuple2<>(count, value) ;
                    }
                }
        );

        // Sort languages by number of repos (descending)
        JavaPairRDD<Integer, String> sorted = countKey.sortByKey(false, 1);

        // Arrange info as required
        JavaRDD<String> infoParts = sorted.map(
                new Function<Tuple2<Integer, String>, String>() {
                    @Override
                    public String call(Tuple2<Integer, String> tup) throws Exception {

                        int count = tup._1;
                        String info = tup._2;

                        String[] tokens = info.split(",");

                        String language = tokens[0];
                        String repoName = tokens[1];
                        String stars = tokens[2];

                        return language + " " + count + " " + repoName + " " + stars;
                    }
                }
        );

        // Make into one file
        JavaRDD<String> info = infoParts.coalesce(1);

        // Save to file
        info.saveAsTextFile("/scr/bfenton/lab9/exp1/output");

        context.stop();
        context.close();

    }

}
