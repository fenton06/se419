import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.*;

public class Checksum {

    public static void main(String[] args) throws Exception {

        // The system configuration
        Configuration conf = new Configuration();

        // Get an instance of the Filesystem
        FileSystem fs = FileSystem.get(conf);

        // Filename path to read
        String open_path = "/class/s17419/lab1/bigdata";

        Path path = new Path(open_path);

        // Create input stream
        FSDataInputStream file = fs.open(path);

        long startOffeset = 5000000000L;
        long endOffset = 5000000999L;
        long num_bytes = endOffset - startOffeset + 1;

        // Seek to specified offset of file
        file.seek(startOffeset);

        // Create read buffer
        byte bytesOut[] = new byte[(int) num_bytes];

        // Read 1000 bytes
        file.read(file.getPos(), bytesOut, 0, (int) num_bytes);

        // Print bytes (test)
        /*int num = 0;

         for(byte b : bytesOut) {
             System.out.println(num + " - " + (Integer.toBinaryString(b & 255 | 256).substring(1)));
             num++;
         }*/

        // Do checksum
        byte checksum = (byte) 0;

        // Find each checksum bit
        for (int i = 0; i < 8; i++) {

            int temp = getBit(bytesOut[0], i) ^ getBit(bytesOut[1], i);

            // XOR each bit in desired bytes
            for (int j = 2; j < bytesOut.length; j++) {

                int next = getBit(bytesOut[j], i);

                temp = temp ^ next;
            }

            // Set corresponding checksum bit if temp == 1
            if (temp == 1) {
                checksum = (byte) (checksum | (1 << i));
            }

        }

        // Convert byte to int
        int checksumInt = checksum & 0xFF;


        // Output checksum value
        //System.out.print(Integer.toBinaryString(checksum & 255 | 256).substring(1) + "\n");
        System.out.print(checksumInt + "\n");

        // Close the file and the file system instance
        file.close();
        fs.close();

    }

    private static int getBit(byte num, int position) {

        return (num >> position) & 1;
    }

}
